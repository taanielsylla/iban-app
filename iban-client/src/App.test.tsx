import React from 'react';
import {render, screen} from '@testing-library/react';
import App from './App';
import Iban from './components/Iban';
import * as ReactDOM from 'react-dom';
import {act} from "react-dom/test-utils";

let container: HTMLDivElement

test('renders header element', () => {
    const {container} = render(<App/>)
    const appBar = container.querySelector(".MuiAppBar-root") // cleaner element searching
    expect(appBar).toBeInTheDocument();
});


describe('IBAN component tests', () => {

    beforeEach(() => {
        container = document.createElement('div');
        document.body.appendChild(container);
        ReactDOM.render(<Iban/>, container);
    })

    afterEach(() => {
        document.body.removeChild(container);
        container.remove();
    })

    it('Renders correctly initial IBAN form', () => {
        const form = container.querySelector('form');
        expect(form).toBeInTheDocument();
    });

    it('renders IBANs list header element', () => {
        const listElem = container.querySelector(".ibans-list-header")
        expect(listElem).toBeInTheDocument();
    });


    /**
     * This is integration test - backend needs to be running for successful test execution
     */
    it('Should show toast message informing empty input if submit pressed', async () => {
        const btn = container.querySelector('form button');
        expect(btn).toBeInTheDocument();

        console.log("WAITING")

        if (btn) {
            await act(async () => {
                btn.dispatchEvent(new MouseEvent('click', {bubbles: true}));
                await new Promise((r) => setTimeout(r, 1000));
            })
        }

        console.log("WAITING DONE")

        const linkElement = screen.getByText(/Please enter IBAN/i);
        expect(linkElement).toBeInTheDocument();
    });

});