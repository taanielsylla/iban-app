# IBAN address validator Full-stack application

![alt text](https://allprowebdesigns.com/blog/wp-content/uploads/2019/06/React-Spring.jpg)

This project consists of the server and the client side applications that can be found from ```iban-server``` and ```iban-client``` folders respectively.

The data is persisted (not in-memory) using H2 database. The database file will be created and stored at the following address:
```
./data/ibanData
```

Additional instructions on starting up the applications and details of configuration properties can be consulted from the README files of the specific projects.

# Production links
UI publicly available at:
```
iban.taanielsylla.com
```
API hosted at:
```
ibanserver.taanielsylla.com
```

Docker containers used for deployment:
```
registry.gitlab.com/taanielsylla/iban-app/client:latest
registry.gitlab.com/taanielsylla/iban-app/server:latest
```

Containers hosted at GitLab container registry. Available at:
```
https://gitlab.com/taanielsylla/iban-app/container_registry/
```

Docker deployment environment: Portainer

![alt text](https://logz.io/wp-content/uploads/2017/01/docker-portainer-1.jpg)

# Development links
Frontend URL:
```
http://localhost:3000/
```
Backend URL:
```
localhost:8080/iban/
```
H2 database connection string:
```
jdbc:h2:file:./data/ibanData;AUTO_SERVER=true
username:sa
Password:password
```
H2 database management console:
```
http://localhost:8080/h2-console/login.jsp
```

Swagger Open API documentation UI:
```
http://localhost:8080/h2-console
```

# Sample API Queries URL's 
Use Postman, insomnia, curl, browser etc

Production:
```
ibanserver.taanielsylla.com/iban/getAll
ibanserver.taanielsylla.com/iban/delete/{id}
ibanserver.taanielsylla.com/iban/validate
```
Development:
```
localhost:8080/iban/iban/getAll
localhost:8080/iban/iban/delete/{id}
localhost:8080/iban/iban/validate
```