package com.testgroup.testsystem.repository;

import com.testgroup.testsystem.model.IbanEntry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IbanRepository extends JpaRepository<IbanEntry,Integer> { // Name of model class & type of primary key
}
