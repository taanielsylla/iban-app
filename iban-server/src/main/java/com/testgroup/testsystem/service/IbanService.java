package com.testgroup.testsystem.service;

import com.testgroup.testsystem.model.IbanEntry;

import java.util.List;

public interface IbanService {

    public IbanEntry saveIban(IbanEntry ibanEntry);

    public List<IbanEntry> getAllIbans(String sortOrder);

    /**
     * Incorporate single & multiple iban validation into one endpoint.
     * @param ibanEntries - List of IBAN's or a single IBAN to validate against
     * @return - List of validation results.
     */
    public List<IbanEntry> validateIban(List<IbanEntry> ibanEntries);

    public void remove(Integer ibanEntryId);
}
