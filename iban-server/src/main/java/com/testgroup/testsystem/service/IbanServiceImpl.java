package com.testgroup.testsystem.service;

import com.testgroup.testsystem.model.IbanEntry;
import com.testgroup.testsystem.repository.IbanRepository;
import nl.garvelink.iban.IBAN;
import nl.garvelink.iban.Modulo97;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * The purpose of this class is to define the business logic for API endpoints.
 */
@Service
public class IbanServiceImpl implements IbanService {

    /**
     * Inject Repository using spring framework
     */
    @Autowired
    private IbanRepository ibanRepository;

    /**
     * Insert new IBAN entry into database.
     *
     * @param ibanEntry - IBAN object
     * @return object that hs bees saved to db
     */
    @Override
    public IbanEntry saveIban(IbanEntry ibanEntry) {
        return ibanRepository.save(ibanEntry);
    }

    /**
     * Return all exiting IBAN records.
     *
     * @param sortOrder - IBAN object
     * @return List of IBAN objects that are currently present in db
     */
    @Override
    public List<IbanEntry> getAllIbans(String sortOrder) {
        if (sortOrder.equals("sort")) return ibanRepository.findAll();
        else {
            Sort.Direction sortDir = getSortDir(sortOrder);
            List<IbanEntry> isValid = ibanRepository.findAll(Sort.by(sortDir, "isValid"));
            return isValid;
        }
    }

    /**
     * Helper function, to get the desired sorting direction of IBAN records
     *
     * @param sortOrder - IBAN object
     * @return Sort direction
     */
    private Sort.Direction getSortDir(String sortOrder) {
        Sort.Direction sortDir;
        if (sortOrder.equals("sort-up")) sortDir = Sort.Direction.ASC;
        else sortDir = Sort.Direction.DESC;
        return sortDir;
    }

    /**
     * Execute validation against all submitted IBAN addresses.
     *
     * @param ibanEntries - List of IBAN's or a single IBAN to validate against
     * @return validation result of all entered IBAN objects
     */
    @Override
    public List<IbanEntry> validateIban(List<IbanEntry> ibanEntries) {

        // Update entity with IBAN validation result
        List<IbanEntry> validated = ibanEntries.stream().peek(e -> {
            int validityInt = isValidIban(e.getIban()) ? 1 : 0;
            e.setIsValid(validityInt);
        }).collect(Collectors.toList());

        // Save result to DB
        ibanRepository.saveAll(validated);

        return ibanEntries;
    }

    /**
     * Delete IBAN record.
     *
     * @param ibanEntryId - IBAN object ID
     */
    @Override
    public void remove(Integer ibanEntryId) {
        ibanRepository.deleteById(ibanEntryId);
    }

    /**
     * Execute IBAN check algorithms to distinguish the validity of an address.
     *
     * @param ibanStr IBAN object
     * @return boolean value of validation result
     */
    private boolean isValidIban(String ibanStr) {
        boolean isValid = false;
        try {
            IBAN iban = IBAN.valueOf(ibanStr);
            String candidate = iban.toString(); // Output example: "NL91 ABNA 0417 1643 00"
            // Use the Modulo97 class directly to compute or verify the check digits on an input.
            isValid = Modulo97.verifyCheckDigits(candidate);
        } catch (Exception e) {
            System.out.printf("IBAN is not valid: %s", ibanStr);
        }

        return isValid;
    }
}
