package com.testgroup.testsystem.controller;

import com.testgroup.testsystem.model.IbanEntry;
import com.testgroup.testsystem.service.IbanService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Here we define endpoints for API
 */
@Api(value = "IBAN RestController")
@RestController
@RequestMapping("/iban")
@CrossOrigin
public class IbanController {

    @Autowired
    private IbanService ibanService;

    @PostMapping("/add")
    public String add(@RequestBody IbanEntry ibanEntry) {
        ibanService.saveIban(ibanEntry);
        return "New IBAN entry is added";
    }

    @DeleteMapping(value = "/remove/{id}")
    public String remove(@PathVariable Integer id) {
        ibanService.remove(id);
        return "deleted";
    }

    /**
     * Validation of single and multiple IBAN addresses separated with commas.
     *
     * @param ibanEntry - The IBAN entry
     * @return List of IBAN objects with values representing the validity of the IBAN check.
     */
    @PostMapping("/validate")
    public List<IbanEntry> validate(@RequestBody List<IbanEntry> ibanEntry) {
        return ibanService.validateIban(ibanEntry);
    }

    @GetMapping("/getAll")
    public List<IbanEntry> list(@RequestParam(required = false, defaultValue = "order", value = "order") String sortOrder) {
        return ibanService.getAllIbans(sortOrder);
    }
}
